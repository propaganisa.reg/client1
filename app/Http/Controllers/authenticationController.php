<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

use Illuminate\Http\Request;

class authenticationController extends Controller
{
    public function index(){
        return view('register');
    }
    public function login(){
        return view('login');
    }


    public function store(Request $request)
    {
        $client = new Client();
        $url = "http://127.0.0.1:8004/api/user/register/";


        $data = array(
       'email' => $request->email,
       'user_name' => $request->username,
       'password' => $request->password,
        );

        $response = $client->request('POST', $url, [
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            'body'    => json_encode($data)
        ]);

        $responseBody = json_decode($response->getBody());

        return view('login');
    }
    public function generateToken(Request $request)
    {
        $client = new Client();
        $url = "http://127.0.0.1:8004/api/token/";


        $data = array(
       'email' => $request->email,

       'password' => $request->password,
        );

        $response = $client->request('POST', $url, [
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            'body'    => json_encode($data)
        ]);


        $data = json_decode($response->getBody());

        
        #print_r($responseBody);
        return view('home')->with('data', $data);
        #return redirect()->route('homepage'); 
    }

    public function accessData()
    {
        return view('accessData');
    }
    public function logout(Request $request)
    {
        
    }

}
