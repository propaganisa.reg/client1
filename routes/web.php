<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\authenticationController;



Route::get('/', function () {
    return view('register');
})->name('register');
Route::get('/login', function () {
    return view('login');
})->name('login');
Route::post('/register', [authenticationController::class, 'store'])->name('store');
Route::post('/generate', [authenticationController::class, 'generateToken'])->name('generate');
Route::post('/homepage', [authenticationController::class, 'homepage'])->name('homepage');
Route::post('/logout', [authenticationController::class, 'logout'])->name('logout');
