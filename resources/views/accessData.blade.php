<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Data API</h2>
  <p>Data was taken form PHP API</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Author</th>
        <th>Content</th>
      </tr>
    </thead>
    
  </table>
</div>
<script type="text/javascript">
        


      const token = localStorage.getItem('access_token');
fetch('http://127.0.0.1:8004/api/', {
  headers: {
    Authorization: `${token}`
  }
})
  .then(res => res.json())
  .then(json => console.log(json));

</script>

</body>
</html>
